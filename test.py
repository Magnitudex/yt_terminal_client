# -*- coding: utf-8 -*-

# Sample Python code for youtube.channels.list
# See instructions for running these code samples locally:
# https://developers.google.com/explorer-help/code-samples#python

import json
import os

from googleapiclient.discovery import build

scopes = ["https://www.googleapis.com/auth/youtube.readonly"]
api_key =os.environ.get('YT_API_KEY')
def main():
    youtube=build('youtube', 'v3', developerKey=api_key)
    request=youtube.search().list(
            part='snippet',
            type='video',
            maxResults=25,
            q="The Pizza Show"
            )
    response=request.execute()
    y=json.dumps(response, indent=4)
    print(y)
if __name__ == "__main__":
    main()
